const Producto = require("../models/Producto");
const { validationResult } = require("express-validator");

exports.crearProducto = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  try {
    //crear un nuevo Producto
    const producto = new Producto(req.body);

  //  producto.creador = req.usuario.id;

    producto.save();
    res.json(producto);
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.obtenerProductos = async (req, res) => {
  try {
    const productos = await Producto.find({ /*creador: req.usuario.id*/ }).sort({
      creado: -1,
    });
    res.json({ productos });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.actualizarProducto = async (req, res) => {
  //revisar si hay errores
  const {  nombre, categoriaMascota, categoriaProducto, precio } = req.body;
  try {
  //  const proyectoEncontrado = await Proyecto.findById(proyecto);
    const productoExiste = await Producto.findById(req.params.id);

    if (!productoExiste) {  return res.status(404).json({ msg: "No existe esa producto" });}

  //  if (!proyectoEncontrado) { return res.status(404).json({ msg: "Proyecto no encontrado" });}

  //  if (proyectoEncontrado.creador.toString() !== req.usuario.id) {  return res.status(400).json({ msg: "No autorizado" });}

    const NuevoProducto = {};
    if (nombre) { NuevoProducto.nombre = nombre;}
    if (categoriaMascota) { NuevoProducto.categoriaMascota = categoriaMascota;}
    if (categoriaProducto) { NuevoProducto.categoriaProducto = categoriaProducto;}
    if (precio) { NuevoProducto.precio = precio;}

    producto = await Producto.findOneAndUpdate(  { _id: req.params.id }, { $set: NuevoProducto }, { new: true } );
    res.json({ producto });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }



  /*
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  const { nombre } = req.body;
  const nuevoProducto = {};

  if (nombre) {
    nuevoProducto.nombre = nombre;
  }

  try {
    let producto = await Producto.findById(req.params.id);


    if (!producto) {
      return res.status(400).json({ msg: "Producto no encontrado" });
    }

    if (producto.creador.toString() !== req.usuario.id) {
      return res.status(400).json({ msg: "No autorizado" });
    }

    producto = await Producto.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: nuevoProducto },
      { new: true }
    );

    res.json({ producto });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }*/
};

exports.eliminarProducto = async (req, res) => {
  try {
    let producto = await Producto.findById(req.params.id);

   /* if (!producto) {
      return res.status(400).json({ msg: "Producto no encontrado" });
    }

    if (producto.creador.toString() !== req.usuario.id) {
      return res.status(400).json({ msg: "No autorizado" });
    }*/

    await Producto.remove({ _id: req.params.id });
    res.json({ msg: "Producto eliminado" });
  } catch (error) {
    console.log("Hubo un error al eliminar el producto");
    console.log(error);
    res.status(400).send("Hubo un error eliminando el producto");
  }
};
